package wCount;

import java.io.*;

public class FileAdapter { // 该类用于文件写入读取的处理

	public String FileToString(File file) throws IOException { // 将文件转化为字符串
		if (!file.exists() || file.isDirectory()) {
			System.out.println("请输入正确文件名！");
			throw new FileNotFoundException();
		}
		FileInputStream fis = new FileInputStream(file);
		byte[] buf = new byte[1024];
		StringBuffer sb = new StringBuffer();
		while ((fis.read(buf)) != -1) {
			sb.append(new String(buf));
			buf = new byte[1024];// 重新生成，避免和上次读取的数据重复
		}
		fis.close();
		return sb.toString();
	}

	public void WriteToFile(String str) throws IOException { // 将传入的字符串写入文件
		File writename = new File("result.txt"); // 相对路径，如果没有则要建立一个新的文件
		writename.createNewFile(); // 创建新文件
		BufferedWriter out = new BufferedWriter(new FileWriter(writename));
		out.write(str);

		out.flush(); // 把缓存区内容压入文件
		out.close(); // 关闭文件

	}

}
