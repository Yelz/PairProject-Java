package wCount;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CountWordFrame  extends JFrame{

	private JLabel label1;
	private JPanel jPanel;
	
	public CountWordFrame(String fileText,int flag,int para) {
		
		this.setTitle("词频统计byYelz");
		this.setSize(800, 600);
		
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dimension = kit.getScreenSize();
		Dimension size = this.getSize();
		int x = (int) (dimension.getWidth() - size.getWidth()) / 2;
		int y = (int) (dimension.getHeight() - size.getHeight()) / 2;
		this.setLocation(x, y);
		
		this.setLayout(new GridLayout(3, 1));
		
		Word wd = new Word(fileText);
		
		int charNum = wd.getCharNum();//字符数量
		int lines = wd.getLines();//有效行
		List<Map.Entry<String, Integer>> sortedWord;
		int wordNum;
		String s = "";
		String strForFile = "";
		if(flag ==0)
		{
			sortedWord=wd.getSortedWord();//得到排序后的存放单词和词频键值对的list
			wordNum = sortedWord.size();
			s += "characters:"+ charNum + "<br/>words:" + wordNum + "<br/>lines:" + lines + "<br/>";
			strForFile += "characters:"+ charNum + "\r\nwords:" + wordNum + "\r\nlines:" + lines + "\r\n";
			int cnt = para;
			if(sortedWord.size()>cnt)
			{
				for (int i = 0; i < cnt; i++)//输出词频最高前十单词
				{
					s +=  "["+sortedWord.get(i).getKey()+"]:"+sortedWord.get(i).getValue() + "<br/>";
					strForFile +=  "["+sortedWord.get(i).getKey()+"]:"+sortedWord.get(i).getValue() + "\r\n";
				}
			}
			else
			{
				for (int i = 0; i < sortedWord.size(); i++)//输出词频最高的单词
				{
					s +=  "["+sortedWord.get(i).getKey()+"]:"+sortedWord.get(i).getValue() + "<br/>";
					strForFile +=  "["+sortedWord.get(i).getKey()+"]:"+sortedWord.get(i).getValue() + "\r\n";
				}
			}
		}
		else
		{
			sortedWord=wd.getLengthSortedWord(para);//得到排序后的存放单词和词频键值对的list
			wordNum = sortedWord.size();
			s = "characters:"+ charNum + "<br/>words:" + wordNum + "<br/>lines:" + lines + "<br/>";
			strForFile += "characters:"+ charNum + "\r\nwords:" + wordNum + "\r\nlines:" + lines + "\r\n";
			for (int i = 0; i < sortedWord.size(); i++)//输出词频最高的单词
			{
				s +=  "["+sortedWord.get(i).getKey()+"]:"+sortedWord.get(i).getValue() + "<br/>";
				strForFile +=  "["+sortedWord.get(i).getKey()+"]:"+sortedWord.get(i).getValue() + "\r\n";
			}
		}
		
		//结果写入result.txt文件
		try {
			FileAdapter fileAdapter = new FileAdapter();
			fileAdapter.WriteToFile(strForFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		jPanel = new JPanel();
		label1 = new JLabel("<html><body>"+s+"</body></html>");
		label1.setFont(new Font("宋体", 1, 25));
		jPanel.add(label1);
		this.add(jPanel);
		
		this.setLayout(new GridLayout(1, 1));
		this.setVisible(true);
		
		
	}
	
	
	
}
