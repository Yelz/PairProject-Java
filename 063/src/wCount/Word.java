package wCount;

import java.util.*;
import java.util.Map.Entry;

public class Word
{

	String text; // 文件中内容
	private int charNum; // 字符个数
	private int wordNum; // 单词总数
	private int lines; // 有效行数
	private Map<String, Integer> wordMap; // 单词词频

	public Word(String text) {
		this.text = text;//在构造函数里传入参数为从文件读入的字符串
	}

	public int getCharNum() // 统计文件字符数(ascll码（32~126），制表符，换行符，)
	{
		char c;
		for (int i = 0; i < text.length(); i++) {
			c = text.charAt(i);
			if (c >= 32 && c <= 126 || c == '\r' || c == '\n'|| c == '\t') {
				charNum++;
			}
		}
		return charNum;
	}


	public boolean isWord(String s)//判断是否是单词
	{
		if(s.matches("[a-zA-Z]{4}[a-zA-Z0-9]*"))//利用正则表达式判断是否是单词(以4个英文字母开头，跟上字母数字符号，单词以分隔符分割，不区分大小写)
			return true;
		else
			return false;
	}
	
	
	//返回一个排序好的存放单词和出现次数的List
	public List getSortedWord() // 统计单词词频(单词：以4个英文字母开头，跟上字母数字符号，单词以分隔符分割，不区分大小写。)
	{
		wordMap = new HashMap<String, Integer>();
		String t = text;
	
		String[] words = t.split("\\s+"); // 对字符串进行分词操作
		for (int i = 0; i < words.length; i++) {
				if (isWord(words[i].trim())) { // 如果是单词
					words[i] = words[i].trim().toLowerCase();//转换为小写
					if (wordMap.get(words[i]) == null) { // 判断之前Map中是否出现过该字符串
						wordMap.put(words[i], 1);//没出现过把这个单词作为key放入map，value值设为1
					} else
						wordMap.put(words[i], wordMap.get(words[i]) + 1);//出现过value值+1
				}
		}
		
		List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(wordMap.entrySet());//把map的键值对装入一个list中
		list.sort(new Comparator<Map.Entry<String, Integer>>() {//对list排序
			@Override
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {	//重写compare方法，对list中内容进行排序，先按词频后按字典顺序
				if (o1.getValue() == o2.getValue()) {//如果词频相等
					return o1.getKey().compareTo(o2.getKey());//就按字典序排列
				}
				return o2.getValue() - o1.getValue();//否则直接按词频排列
			}

		});
		
		return list;//返回排列好后的list
	}
	
	public List getLengthSortedWord(int len)
	{
		List<Map.Entry<String, Integer>> list = getSortedWord();
		List<Map.Entry<String, Integer>> res = new ArrayList<>();
		for(int i=0;i<list.size();i++)
		{
			if(list.get(i).getKey().length()==len)
				res.add(list.get(i));
		}
		return res;
	}

	public int getLines() { // 统计有效行数
		String[] line = text.split("\r\n"); // 将每一行分开放入一个字符串数组
		for (int i = 0; i < line.length; i++) { // 找出无效行，统计有效行
			if (line[i].trim().length() == 0)//trim函数删除字符串的头尾空白符
				continue;
			lines = lines + 1;
		}
		return lines;
	}


}
