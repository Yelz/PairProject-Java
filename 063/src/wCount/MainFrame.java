package wCount;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MainFrame  extends JFrame implements ActionListener{
	
	private JButton chooseFile,topCount,lenCount;
	private TextField topText,lenText;
	private JPanel jPanel1,jPanel2,jPanel3;
	private JLabel fileName,topTip,lenTip;
	private String fileText;
	private File file;
	public MainFrame() {
		this.setTitle("词频统计byYelz");
		this.setSize(800, 600);
		
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dimension = kit.getScreenSize();
		Dimension size = this.getSize();
		int x = (int) (dimension.getWidth() - size.getWidth()) / 2;
		int y = (int) (dimension.getHeight() - size.getHeight()) / 2;
		this.setLocation(x, y);
		
		this.setLayout(new GridLayout(3, 1));
		
		chooseFile = new JButton("选择文件");
		chooseFile.addActionListener(this);
		topCount = new JButton("按词频排序前n单词");
		topCount.addActionListener(this);
		lenCount = new JButton("按单词长度和词频排序");
		lenCount.addActionListener(this);
		
		topText = new TextField(10);
		lenText = new TextField(10);
		
		topTip = new JLabel("统计词频前几的单词？");
		lenTip = new JLabel("请输入想统计的单词长度：");
		
		
		jPanel1 = new JPanel();
		jPanel2 = new JPanel();
		jPanel3 = new JPanel();
		
		jPanel1.add(chooseFile);
		this.add(jPanel1);
		
		jPanel2.add(topTip);
		jPanel2.add(topText);
		jPanel2.add(topCount);
		this.add(jPanel2);
		
		jPanel3.add(lenTip);
		jPanel3.add(lenText);
		jPanel3.add(lenCount);
		this.add(jPanel3);
		
		this.fileText="";
		this.file = null;
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO 自动生成的方法存根
		if(e.getSource() == chooseFile)
		{
			try {
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("打开txt文件");
				int result = fc.showOpenDialog(this);  // 打开"打开文件"对话框
				// int result = dlg.showSaveDialog(this);  // 打"开保存文件"对话框
				if (result == JFileChooser.APPROVE_OPTION) {
					file = fc.getSelectedFile();
				}
				if(file!=null)
				{
					FileAdapter fd = new FileAdapter();
					String text = fd.FileToString(file);
					this.fileText=text;
//					System.out.println(fileText);
					if(fileName!=null)
						jPanel1.remove(1);
					fileName = new JLabel("已选文件名："+file.getName());
					jPanel1.add(fileName,BorderLayout.WEST);
					this.repaint();
					this.validate();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		if(e.getSource() == topCount)
		{
			if(file != null)
			{
					if(topText.getText().equals("") || topText.getText()==null)
				{
					JOptionPane.showMessageDialog(null, "请输入整数确定显示前几单词", "错误", JOptionPane.ERROR_MESSAGE);
				}
				else if(!isInteger(topText.getText()))
				{
					JOptionPane.showMessageDialog(null, "请输入整数", "错误", JOptionPane.ERROR_MESSAGE);
				}
				else {
					new CountWordFrame(fileText, 0, Integer.parseInt(topText.getText()));
					topText.setText("");
					//this.dispose();
				}
			}
			else {
				JOptionPane.showMessageDialog(null, "请先选择文件！", "错误", JOptionPane.ERROR_MESSAGE);
			}
			
		}
		if(e.getSource()==lenCount){
			if(file != null)
			{
				if(lenText.getText().equals("") || lenText.getText()==null)
				{
					JOptionPane.showMessageDialog(null, "请输入整数确定显示前几单词", "错误", JOptionPane.ERROR_MESSAGE);
				}
				else if(!isInteger(lenText.getText()))
				{
					JOptionPane.showMessageDialog(null, "请输入整数", "错误", JOptionPane.ERROR_MESSAGE);
				}
				else {
					new CountWordFrame(fileText, 1, Integer.parseInt(lenText.getText()));
					lenText.setText("");
					//this.dispose();
				}
			}
			else {
				JOptionPane.showMessageDialog(null, "请先选择文件！", "错误", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	  public static boolean isInteger(String str) {  
	        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");  
	        return pattern.matcher(str).matches();  
	  }
}
